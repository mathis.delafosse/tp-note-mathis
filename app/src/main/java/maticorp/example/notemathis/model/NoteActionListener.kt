package maticorp.example.notemathis.model

interface NoteActionListener {
    fun onNoteEditClick(note: Note)
    fun onNoteDeleteClick(note: Note)
}