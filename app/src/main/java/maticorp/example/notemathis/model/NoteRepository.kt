package maticorp.example.notemathis.model

import androidx.lifecycle.LiveData

class NoteRepository(
    private val noteDao: NoteDao
) {
    suspend fun insertNote(word: Note) {
        noteDao.insertNote(word)
    }

    suspend fun updateNote(note: Note) {
        noteDao.updateNote(note)
    }

    fun findNoteById(id: Int): LiveData<Note> {
        return noteDao.findNoteById(id)
    }

    suspend fun deleteNote(note: Note) {
        noteDao.deleteNote(note)
    }


    fun getAllNotes(): LiveData<List<Note>> {
        return noteDao.getAllNotes()
    }
}