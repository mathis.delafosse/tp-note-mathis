package maticorp.example.notemathis.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Note::class], version = 1)
abstract class NoteDatebase: RoomDatabase() {
    abstract fun getNoteDao(): NoteDao
    companion object {
        private var NOTE_DATABASE: NoteDatebase? = null

        fun getNoteDatabase(context: Context): NoteDatebase {
            if (NOTE_DATABASE == null) {
                NOTE_DATABASE = Room.databaseBuilder(context,
                    NoteDatebase::class.java,
                    "noteDb"
                ).build()
            }
            return NOTE_DATABASE!!
        }
    }
}