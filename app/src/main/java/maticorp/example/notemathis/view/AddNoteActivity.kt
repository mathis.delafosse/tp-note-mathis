package maticorp.example.notemathis.view

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import maticorp.example.notemathis.NoteApplication
import maticorp.example.notemathis.R
import maticorp.example.notemathis.model.Note
import maticorp.example.notemathis.viewmodel.NoteViewModel
import maticorp.example.notemathis.viewmodel.NoteViewModelFactory

class AddNoteActivity : AppCompatActivity() {

    private lateinit var noteViewModel: NoteViewModel
    private var noteId: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_note)

        val titleEditText: EditText = findViewById(R.id.edit_note_title)
        val contentEditText: EditText = findViewById(R.id.edit_note_content)
        val saveButton: Button = findViewById(R.id.save_button)

        noteViewModel = ViewModelProvider(this, NoteViewModelFactory(
            (application as NoteApplication).repository
        )).get(NoteViewModel::class.java)

        // Get note info from intent
        val extras = intent.extras
        if (extras != null) {
            this.noteId = extras.getInt("id")
            val title = extras.getString("title")
            val content = extras.getString("content")

            titleEditText.setText(title)
            contentEditText.setText(content)
        }

        // On Save button click
        saveButton.setOnClickListener {
            val noteTitle: EditText = findViewById(R.id.edit_note_title)
            val noteText: EditText = findViewById(R.id.edit_note_content)
            val title = noteTitle.text.toString()
            val text = noteText.text.toString()

            if (title.isNotEmpty() && text.isNotEmpty()) {
                // Update note
                if(this.noteId != null) {
                    val note = Note(id = this.noteId!!, title = title, content = text)
                    noteViewModel.updateNote(note)
                }
                // Or Create note
                else {
                    val note = Note(title = title, content = text)
                    noteViewModel.insertNote(note)
                }
                finish()
            } else {
                Toast.makeText(this, "Please enter title and content", Toast.LENGTH_SHORT).show()
            }
        }
    }
}