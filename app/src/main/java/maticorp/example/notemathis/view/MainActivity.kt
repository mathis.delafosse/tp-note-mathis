package maticorp.example.notemathis.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import maticorp.example.notemathis.NoteApplication
import maticorp.example.notemathis.R
import maticorp.example.notemathis.model.Note
import maticorp.example.notemathis.model.NoteActionListener
import maticorp.example.notemathis.viewmodel.NoteViewModel
import maticorp.example.notemathis.viewmodel.NoteViewModelFactory

class MainActivity : AppCompatActivity(), NoteActionListener {

    private lateinit var noteViewModel: NoteViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        val adapter = NoteAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        noteViewModel = ViewModelProvider(this, NoteViewModelFactory(
            (application as NoteApplication).repository
        )).get(NoteViewModel::class.java)

        noteViewModel.allNotes.observe(this, Observer { notes ->
            adapter.setNotes(notes)
        })

        // Add Note Button
        val addNewWord: FloatingActionButton = findViewById(R.id.addNewWord)
        addNewWord.setOnClickListener {
            val intent = Intent(this, AddNoteActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onNoteEditClick(note: Note) {
        val intent = Intent(this, AddNoteActivity::class.java)
        intent.putExtra("id", note.id)
        intent.putExtra("title", note.title)
        intent.putExtra("content", note.content)
        startActivity(intent)
    }

    override fun onNoteDeleteClick(note: Note) {
        noteViewModel.deleteNote(note)
    }
}