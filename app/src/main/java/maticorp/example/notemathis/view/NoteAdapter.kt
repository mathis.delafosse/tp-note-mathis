package maticorp.example.notemathis.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import maticorp.example.notemathis.R
import maticorp.example.notemathis.model.Note
import maticorp.example.notemathis.model.NoteActionListener

class NoteAdapter(
    noteActionListener: NoteActionListener
) : RecyclerView.Adapter<NoteAdapter.NoteViewHolder>() {
    private var notes = emptyList<Note>()
    private var listener = noteActionListener

    inner class NoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val noteTitle: TextView = itemView.findViewById(R.id.noteTitle)
        val noteText: TextView = itemView.findViewById(R.id.noteContent)
        val editButton: Button = itemView.findViewById(R.id.noteEditButton)
        val deleteButton: Button = itemView.findViewById(R.id.noteDeleteButton)

        fun bind(note: Note) {
            editButton.setOnClickListener {
                listener.onNoteEditClick(note)
            }

            deleteButton.setOnClickListener {
                listener.onNoteDeleteClick(note)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.note_item, parent, false)
        return NoteViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        val currentNote = notes[position]
        holder.noteTitle.text = currentNote.title
        holder.noteText.text = currentNote.content
        holder.bind(currentNote)
    }

    override fun getItemCount(): Int {
        return notes.size
    }

    fun setNotes(notes: List<Note>) {
        this.notes = notes
        notifyDataSetChanged()
    }
}