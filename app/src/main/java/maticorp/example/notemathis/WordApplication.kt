package maticorp.example.notemathis

import android.app.Application
import maticorp.example.notemathis.model.NoteDatebase
import maticorp.example.notemathis.model.NoteRepository

class NoteApplication : Application() {
    val db by lazy { NoteDatebase.getNoteDatabase(this) }
    val repository by lazy { NoteRepository(db.getNoteDao()) }
}