package maticorp.example.notemathis.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import maticorp.example.notemathis.model.NoteRepository
import java.lang.IllegalArgumentException

class NoteViewModelFactory(
    private val repository: NoteRepository
): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NoteViewModel::class.java)) {
            return NoteViewModel(repository) as T
        }

        return throw IllegalArgumentException("Invalid argument")
    }
}