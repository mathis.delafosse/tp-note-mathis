package maticorp.example.notemathis.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import maticorp.example.notemathis.model.Note
import maticorp.example.notemathis.model.NoteRepository

class NoteViewModel (
    private val repository: NoteRepository
): ViewModel() {

    private var _allNotes: LiveData<List<Note>> = repository.getAllNotes()
    val allNotes: LiveData<List<Note>> = _allNotes

    fun insertNote(note: Note) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertNote(note)
    }

    fun updateNote(note: Note) = viewModelScope.launch(Dispatchers.IO) {
        repository.updateNote(note)
    }

    fun findNoteById(id: Int): LiveData<Note> {
        return repository.findNoteById(id)
    }

    fun deleteNote(word: Note) = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteNote(word)
    }
}